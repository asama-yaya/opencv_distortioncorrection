#include "main.h"



int main(int argc, char *argv[]){

	/***********************************************************************/
	// capture
	/***********************************************************************/

	// 初期化
	webCamera.init(0);
	cv::Mat image = webCamera.capture();
	camera_width = image.cols;
	camera_height = image.rows;

	cv::Mat timg;
	cv::resize(image, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
	cv::imshow("image", timg);
	cv::waitKey(10);

	cv::setMouseCallback("image", my_mouse_callback, (void *)&timg);

	// 内部パラメータの読み込み
	cv::Mat intrinsic, distortion;
	std::string  fileName1 = "data\\CAM_PARAM_MAT.xml";
	cv::FileStorage tcvfs(fileName1, cv::FileStorage::READ);
	cv::FileNode param1(tcvfs.fs, NULL);
	cv::read(tcvfs["CameraMatrix"], intrinsic);
	cv::read(tcvfs["DistortCoeffs"], distortion);
	tcvfs.release();

	// マップの作成
	cv::Mat MapX, MapY;
	cv::Mat mapR = cv::Mat::eye(3, 3, CV_64F);
	cv::Mat new_intrinsic = cv::getOptimalNewCameraMatrix(intrinsic, distortion, image.size(), 0);
	cv::initUndistortRectifyMap(intrinsic, distortion, mapR, new_intrinsic, image.size(), CV_32FC1, MapX, MapY);
	cout << intrinsic << endl;
	cout << new_intrinsic << endl;

	cv::Mat dist1, dist2;

	// 撮影
	while (true){
		image = webCamera.capture();
		cv::resize(image, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
		cv::imshow("image", timg);

		cv::undistort(image, dist1, intrinsic, distortion);
		cv::resize(dist1, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
		cv::imshow("dist1", timg);

		cv::remap(image, dist2, MapX, MapY, cv::INTER_LINEAR);
		cv::resize(dist2, timg, cv::Size(), 640.0 / (double)camera_width, 640.0 / (double)camera_width);
		cv::imshow("dist2", timg);

		char key = cv::waitKey(10);
		if (key == '\033' || key == 'q'){
			break;
		}
		else if (key == 'c'){
			cv::imwrite("data\\image.png", image);
			cv::imwrite("data\\dist1.png", dist1);
			cv::imwrite("data\\dist2.png", dist2);
			cout << "capture!" << endl;
		}
	}
	cout << "おしり" << endl;
	return 0;
}


// コールバック関数
void my_mouse_callback(int event, int x, int y, int flags, void* param){
	cv::Mat* image = static_cast<cv::Mat*>(param);

	switch (event){

	case cv::EVENT_RBUTTONDOWN:
		captureFlag = true;
		break;
	default:
		break;
	}
}





















