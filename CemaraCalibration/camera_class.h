#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定

class TWebCamera{

public:
	TWebCamera(){}
	~TWebCamera(){}

private:
	cv::VideoCapture cap;
	cv::Mat image;

public:

	// 初期化
	void init(int _cameraID){
		cap.open(_cameraID);
		if (!cap.isOpened()){
			std::cout << "Error: we can't open webcam" << std::endl;
			return;
		}

		cv::Mat frame;
		for (int ii = 0; ii < 5; ii++)
			cap >> frame;
		image = frame.clone();
		std::cout << frame.cols << " x " << frame.rows << std::endl;
	};

	// 撮影
	cv::Mat capture(){
		cap >> image;
		return image;
	};

};